<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register</title>
<link href="webjars/bootstrap/4.6.0/css/bootstrap.min.css" rel="stylesheet">

<style>

	.login-form{
		width: 400px;
		height: 350px;
		position: absolute;
		background-color: black;
		border-radius: 20px;
		top: 50%;
		left: 50%;
		margin-right: -50%;
		transform: translate(-50%,-50%);
	
	
	}


</style>
</head>
<body>

<div class="login-form">
	
		
	
		<div class="container-fluid">
			<form action="/Pozoriste/reg" method="post">
			
				<div class="mt-3">
					<input type="text" name="ime" class="form-control " placeholder="Ime" />
				</div>
				 
				 <div class="mt-3">
				 	<input type="text" name="prezime" class="form-control " placeholder="Prezime" /> 
				 </div> 
				 
				 <div class="mt-3">
					<input type="text" name="email" class="form-control " placeholder="email" />
				</div>
				
				<div class="mt-3">
					<input type="text" name="username" class="form-control " placeholder="username" />
				</div>
				
				<div class="mt-3">
					<input type="password" name="password"  class="form-control " placeholder="password" />
				</div>
				 
				 
				<button type="submit" class="btn btn-dark btn-block  mt-3">Register</button>
				
				
				
				
			</form>
		
		
		
		
		</div>
	</div>

</body>
</html>