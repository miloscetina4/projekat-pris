<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<link href="webjars/bootstrap/4.6.0/css/bootstrap.min.css" rel="stylesheet">

<style>

	.login-form{
		width: 400px;
		height: 250px;
		position: absolute;
		background-color: black;
		border-radius: 20px;
		top: 50%;
		left: 50%;
		margin-right: -50%;
		transform: translate(-50%,-50%);
	
	
	}


</style>

</head>
<body>
	
	
	<div class="login-form">
	
		<c:if test="${not empty errorMsg }">
			<div class="alert alert-danger" role="alert">${errorMsg}</div>
		
		
		</c:if>
	
		<div class="container-fluid">
			<form method="post">
			
				<div class="mt-3">
					<input type="text" name="userId" class="form-control " placeholder="User ID" />
				</div>
				 
				 <div class="mt-3">
				 	<input type="password" name="password" class="form-control " placeholder="Password" /> 
				 </div> 
				<button type="submit" class="btn btn-dark btn-block  mt-3">login</button>
				
				
				<div class="mt-3">
					<a href="/Pozoriste/register.jsp" class="btn btn-success btn-block mt-3">register</a>
				
				</div>
				
			</form>
		
		
		
		
		</div>
	</div>
</body>
</html>