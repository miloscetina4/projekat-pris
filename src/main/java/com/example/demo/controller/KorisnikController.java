package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.repository.korisnikRepository;

import model.Korisnik;

@Controller
@RequestMapping(value="")
public class KorisnikController {
	
	
	@Autowired
	korisnikRepository kr;

	
	@RequestMapping(value="login",method=RequestMethod.GET)
	public String loginPage() {
		return "login";
	}
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String welcomePage(ModelMap model,HttpServletRequest request) {
//		if(userId.equals("admin")&& password.equals("root")) {
//			model.put("userId", userId);
//			return "welcome";
//		}
		
		String username=request.getParameter("userId");
		String password=request.getParameter("password");
		Korisnik k=kr.findByUsernameAndPassword(username, password);
		if(k!=null) {
			return "welcome";
		}else {
		
		
			model.put("errorMsg", "Unesite validne podatke");
			return "login";
		}
	}
	
	@RequestMapping(value="reg",method=RequestMethod.POST)
	public String registerPage(Model model,HttpServletRequest request,String ime,String prezime,String username,String password,String email) {
		Korisnik k=new Korisnik();
		
		k.setIme(ime);
		k.setPrezime(prezime);
		k.setUsername(username);
		k.setPassword(password);
		k.setEmail(email);
		
		kr.save(k);
		
		return "login";
		
		
		
		//model.addAttribute("korisnik",k);
		
		
	}
	
}
