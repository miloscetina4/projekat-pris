package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Korisnik;

public interface korisnikRepository extends JpaRepository<Korisnik,Integer>{
		Korisnik findByUsernameAndPassword(String user,String password);

}
