package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Korisnik;

public interface pozoristeUserRepository extends JpaRepository<Korisnik,Integer>{
	Korisnik findByUsername(String username);
}
